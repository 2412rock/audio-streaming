#include <stdio.h>

#include "library.h"

/* library's initialization function */
void _init()
{
	printf("Initializing library\n");
}

int encode(char* buffer, int bufferlen)
{
	return bufferlen;
}

char *decode(char* buffer, int bufferlen, int *outbufferlen)
{  
	*outbufferlen = bufferlen;
	return buffer;
}


/* library's cleanup function */
void _fini()
{
	printf("Cleaning out library\n");
}

