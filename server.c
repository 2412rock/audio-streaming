#include <arpa/inet.h>
#include <dlfcn.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <resolv.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "library.h"
#include "audio.h"
#include "message.h"


#define WAITING_TIME 1
#define BUFSIZE 1024
#define PORT 1234


static int breakloop = 0;


int stream_data(int client_fd, struct sockaddr_in *addr, socklen_t *addr_len){
	int audio_data, error;
	int channels, sample_size, sample_rate;
	char *audio_file, *libfile;
	char buffer[BUFSIZE];

	fd_set read_set;
  	struct timeval timeout;
	struct file_settings file_message;
	struct audio_settings audio_message;
	struct _chunk audio_chunk;

	error = recvfrom(client_fd, &file_message, (size_t) sizeof(file_message), 0,
                 (struct sockaddr*) addr, addr_len);
	if(error<0){
		exit(EXIT_FAILURE);
	}

	audio_file = strdup(file_message.file);
	
	if(strcmp(file_message.lib,NO_LIB_REQUESTED)){
		libfile = NULL;	
	}else{
		libfile = file_message.lib;
	}

	audio_data = aud_readinit(audio_file, &sample_rate, &sample_size, &channels);
	if (audio_data < 0){
		printf("file not found\n",audio_file);
		return -1;
	}


	audio_message.channels = channels;
	audio_message.size = sample_size;
	audio_message.rate = sample_rate;
	
	error = sendto(client_fd, &audio_message, sizeof(struct audio_settings), 0,(struct sockaddr*) addr, sizeof(struct sockaddr_in));

	if(error < 0){
		exit(EXIT_FAILURE);
	}
	audio_chunk.count = 1;
	int counter = 0;
	int  lost_packets = 0;
	int bytes_read_from_file = read(audio_data,audio_chunk.buffer,sizeof(audio_chunk.buffer));
	
	audio_chunk.length = bytes_read_from_file;

	while(bytes_read_from_file > 0){
		error  = sendto(client_fd, &audio_chunk, sizeof(struct _chunk), 0,(struct sockaddr*) addr, sizeof(struct sockaddr_in));
		if(error < 0){
			exit(EXIT_FAILURE);
		}
  		FD_ZERO(&read_set);
  		FD_SET(client_fd, &read_set);
  		timeout.tv_sec = 1;
  		timeout.tv_usec = 0;
  		error = select(client_fd+1, &read_set, NULL, NULL, &timeout);
  		if (error < 0 && breakloop != 0) {
    		//gracefully_close
    		if(client_fd >= 0 ){
    			close(client_fd);
    		}if(audio_data >= 0){
    			close(audio_data);
    		}
    		exit(EXIT_SUCCESS);
  		}

      	if(error < 0){
			exit(EXIT_FAILURE);
		}
       
		if(error == 0) lost_packets++;

		if (FD_ISSET(client_fd, &read_set)) {
			error = recvfrom(client_fd, &counter, sizeof(counter), 0, (struct sockaddr*) addr, addr_len);
        	if(error<0){
        		exit(EXIT_FAILURE);
        	}
		}

		bytes_read_from_file = read(audio_data,audio_chunk.buffer,sizeof(audio_chunk.buffer));
		audio_chunk.length = bytes_read_from_file;
		audio_chunk.count +=1;
	
	}
	return 0;
}


void sigint_handler(int sigint)
{
	if (!breakloop){
		breakloop=1;
		printf("SIGINT catched. Please wait to let the server close gracefully.\nTo close hard press Ctrl^C again.\n");
	}
	else{
       		printf ("SIGINT occurred, exiting hard... please wait\n");
		exit(-1);
	}
}

int main (int argc, char **argv){
	signal(SIGINT, sigint_handler );	// trap Ctrl^C signals

	int fd, err;
	struct sockaddr_in addr;


    fd_set read_set;
    socklen_t addr_len = sizeof(struct sockaddr_in);
  	fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);


  	if(fd<0){
  		exit(EXIT_FAILURE);
  	}

  	addr.sin_family = AF_INET;
  	addr.sin_port = htons(PORT);
  	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	err = bind(fd, (struct sockaddr *) &addr, sizeof(struct sockaddr_in));
	if(err<0){
		exit(EXIT_FAILURE);
	}
	printf("Audio server is running on port %d ... \n",PORT);

	
	while (!breakloop){

		printf("waiting ... \n");

 		err = stream_data(fd, &addr, &addr_len);

		sleep(1);
	}

	close(fd);
  	return 0;
}

