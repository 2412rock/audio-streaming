#include <limits.h>

#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#define NAME_MAX 255
#define BUFSIZE 1024 // reccomended maximum UDP Payload
#define SUCCESS 200
#define FILE_NOT_FOUND 404
#define LIB_NOT_FOUND 500
#define NO_LIB_REQUESTED "/" 


struct file_settings {
  char file[NAME_MAX+1];  // plus one since NAME_MAX doesn't contain '\0'
  char lib[NAME_MAX+1];
};

struct audio_settings {
  int channels,size, rate, error;
};

struct __attribute__((packed)) _chunk {
  char buffer[BUFSIZE];
  int length;
  unsigned int count;
};


struct ack{
  int count;
};
#endif
