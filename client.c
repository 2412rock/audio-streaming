#include <arpa/inet.h>
#include <dlfcn.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <resolv.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "library.h"
#include "audio.h"
#include "message.h"


#define SERVER 1234
#define WAITING_TIME 1
#define NO_RESPONSE_LIMIT 99


static int breakloop = 0;

void sigint_handler(int sigint)
{
	if (!breakloop){
		breakloop=1;
		printf("SIGINT catched. Please wait to let the client close gracefully.\nTo close hard press Ctrl^C again.\n");
	}
	else{
       	printf ("SIGINT occurred, exiting hard... please wait\n");
		exit(-1);
	}
}


int main (int argc, char *argv []){
	int server_fd, audio_fd, err;
	int sample_size, sample_rate, channels;
	client_filterfunc pfunc;
	char buffer[BUFSIZE];
	fd_set read_set;

  	struct timeval timeout;
	struct sockaddr_in server_socket;
    struct in_addr *server_address;
    socklen_t server_socket_len = sizeof(struct sockaddr_in);

	struct file_settings file_message;
	struct audio_settings audio_message;
	struct _chunk this_audio_chunk;

	if (argc < 3){
		printf ("error : called with incorrect number of parameters\nusage : %s <server_name/IP> <filename> [<filter> [filter_options]]]\n", argv[0]) ;
		return -1;
	}

	// trap Ctrl^C signals
	signal( SIGINT, sigint_handler );	
	

	server_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (server_fd < 0){
		printf("error: unable to connect to server.\n");
		return -1;
	}

	struct hostent *resolve = gethostbyname(argv[1]);
 	
	 if (resolve == NULL) {
    	fprintf(stderr, "Error while resolving address. \n");
    	exit(EXIT_FAILURE);
  	}
  	
	server_address =  (struct in_addr*) resolve->h_addr_list[0];

	server_socket.sin_family = AF_INET;
	server_socket.sin_port = htons(SERVER);
	server_socket.sin_addr = *server_address;

	strncpy(file_message.file, argv[2], NAME_MAX);
  	if (argc == 4) {
    	strncpy(file_message.lib, argv[3], NAME_MAX);
  	} else {
    	strncpy(file_message.lib,  NO_LIB_REQUESTED, NAME_MAX);
  	}

  	err = sendto(server_fd, &file_message, sizeof(struct file_settings), 0, (struct sockaddr*) &server_socket, sizeof(server_socket));
  	if(err<0){
  		exit(EXIT_FAILURE);
  	}

  	FD_ZERO(&read_set);
  	FD_SET(server_fd, &read_set);
  	
	err = select(server_fd+1, &read_set, NULL, NULL, NULL);
  
  	if (err < 0 ) {
    	exit(EXIT_FAILURE);

  	}

	
   	err = recvfrom(server_fd, &audio_message, sizeof(audio_message), 0, (struct sockaddr*) &server_socket, &server_socket_len);
	if(err<0){
		exit(EXIT_FAILURE);
	}
  	
	audio_fd = aud_writeinit(audio_message.rate, audio_message.size, audio_message.channels);

	if (audio_fd < 0){
		printf("error: unable to open audio output.\n");
		return -1;
	}
	
	int bytesread;
	int len = BUFSIZE;
    int  no_response = 0;
    
    while (len >= BUFSIZE) {
    	if (breakloop != 0) {
			//  gracefully_close(server_fd,audio_fd);
			  if(server_fd >= 0 ){
    			close(server_fd);
    		}if(audio_fd >= 0){
    			close(audio_fd);
    		}
    		exit(EXIT_SUCCESS);

		}

		FD_ZERO(&read_set);
  		FD_SET(server_fd, &read_set);
  		timeout.tv_sec = 1;
  		timeout.tv_usec = 0;
  		err = select(server_fd+1, &read_set, NULL, NULL, &timeout);
  		if (err < 0 ) {
    		
    		exit(EXIT_FAILURE);

  		}
 
		if (err == 0) {
			no_response++;
				}
			
			if (no_response > NO_RESPONSE_LIMIT) {
				fprintf(stderr, "The server is not responding any more. Aborting.\n");
				return -1;

		} else if(FD_ISSET(server_fd, &read_set)) {
			no_response = 0;
			
			bytesread = recvfrom(server_fd, &this_audio_chunk, sizeof(struct _chunk), 0, (struct sockaddr*) &server_socket, &server_socket_len);

			if(bytesread<0){
				exit(EXIT_FAILURE);
			}

			err = sendto(server_fd, &this_audio_chunk.count, sizeof(this_audio_chunk.count), 0, (struct sockaddr*) &server_socket, sizeof(server_socket));
			if(err<0){
				exit(EXIT_FAILURE);
			}
			len = this_audio_chunk.length;
				

			err = write(audio_fd, this_audio_chunk.buffer, sizeof(this_audio_chunk.buffer));
			if(err<0){
				exit(EXIT_FAILURE);
			}


		}
	}
	return 0;
}

